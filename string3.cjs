let findMonth = (string) => {
    if (typeof string !== 'string') {
        return [];
    }
    let arr = string.split("/");
    let month = arr[1];
    if (month > 12) return [];

    month = month % 13;

    let arrayOfMonths = ["[]", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    return arrayOfMonths[month];
}

module.exports = findMonth;