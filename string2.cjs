let converIntoarray = (ipAddress) => {
    if (typeof ipAddress !== 'string') {
        return [];
    }
    let arr = [];
    const regexExp = (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/);

    let answer = regexExp.test(ipAddress);

    if (answer) {
        arr = ipAddress.split(".");
    }
    else return [];

    for (let index = 0; index < arr.length; index++) {
        arr[index] = (+arr[index]);
    }
    return arr;

}
module.exports = converIntoarray;