const convertIntoNumeric = (input) => {    
    if (typeof(input) !== 'string') {
        return 0;
    }
    let answer = 0;
    input = input.trim();
    input = input.replaceAll(',', '');
    input = input.replaceAll('$', '');
    answer = (+input);
    return answer;
}
module.exports = convertIntoNumeric;