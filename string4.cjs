let convertIntoFullName = (obj) => {
    if (obj === null || obj === undefined || typeof obj !== "object" || Array.isArray(obj)) {
        return [];
    }
    let firstName = obj.first_name;
    let middleName = obj.middle_name;
    let lastName = obj.last_name;
    if (firstName == undefined) firstName = "";
    if (middleName == undefined) {
        middleName = "";
    }
    else {
        middleName = middleName + " ";
    }
    if (lastName == undefined) lastName = "";

    let FullName = (firstName.charAt(0).toUpperCase() + firstName.slice(1).toLowerCase()) + " " + (middleName.charAt(0).toUpperCase() + middleName.slice(1).toLowerCase()) + (lastName.charAt(0).toUpperCase() + lastName.slice(1).toLowerCase());

    return FullName;

}

module.exports = convertIntoFullName;
