let converIntoString = (arr) => {
    if (!Array.isArray(arr) || arr.length === 0) {
        return [];
    }
    let ans = ""
    ans = arr.toString();
    ans = ans.replaceAll(",", " ");
    return ans;
}
module.exports = converIntoString;